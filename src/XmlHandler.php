<?php

namespace Drupal\wp_modula_gallery_xml_media_import;

use Drupal\file\FileRepositoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Xml file Handler.
 */
class XmlHandler {

  /**
   * The file system object.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository object.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * ModalFormContactController constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The form builder.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository object.
   */
  public function __construct(FileSystemInterface $file_system, FileRepositoryInterface $file_repository) {
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): XmlHandler {
    return new static(
      $container->get('file_system'),
      $container->get('file.repository')
    );
  }

  /**
   * Converts the xml file to an SimpleXMLElement.
   *
   * @param string $fileName
   *   The file name.
   *
   * @return \SimpleXMLElement
   *   The converted Crawler.
   */
  public function convertXmlFileToObject(string $fileName): \SimpleXMLElement {
    $filePath = 'private://' . $fileName;
    $xmlStr = file_get_contents($filePath);
    if (empty($xmlStr)) {
      throw new \Exception('The file could not be found!');
    }
    return new \SimpleXMLElement($xmlStr, LIBXML_NOCDATA);
  }

  /**
   * Fetches the gallerie items from an xml file.
   *
   * @param \SimpleXMLElement $xml
   *   The xml object.
   * @param string $galleriesXPath
   *   The galleries xpath selector.
   *
   * @return array
   *   The gallerie items.
   */
  public function fetchGalleriesFromXml(\SimpleXMLElement $xml, string $galleriesXPath = "/rss/channel/item[wp:post_type='modula-gallery']"): array {
    $modulaGalleriesXmlArray = $xml->xpath($galleriesXPath);

    $modulaGalleriesArray = [];
    if (!empty($modulaGalleriesXmlArray)) {
      foreach ($modulaGalleriesXmlArray as $modulaGalleryXmlElement) {
        $modulaGalleryArray = (array) $modulaGalleryXmlElement->children('wp', TRUE);
        $modulaGalleryArray = array_merge($modulaGalleryArray, (array) $modulaGalleryXmlElement->children());

        // Add image IDs (serialized in XML):
        /* @phpstan-ignore-next-line */
        $modulaGalleryImagesBlob = (string) $modulaGalleryXmlElement->xpath("wp:postmeta[wp:meta_key='modula-images']/wp:meta_value")[0];
        $modulaGalleryImageArray = unserialize($modulaGalleryImagesBlob);
        $modulaGalleryImageIdArray = [];
        foreach ($modulaGalleryImageArray as $modulaGalleryImage) {
          // We only need the image IDs:
          $modulaGalleryImageId = (int) $modulaGalleryImage['id'];
          $modulaGalleryImageIdArray[$modulaGalleryImageId] = $modulaGalleryImageId;
        }
        $postMetaArray = [];
        foreach ($modulaGalleryArray['postmeta'] as $postmeta) {
          $postMetaArray[(string) $postmeta->children('wp', TRUE)->meta_key] = (string) $postmeta->children('wp', TRUE)->meta_value;
        }
        $postMetaArray['modula-images'] = unserialize($postMetaArray['modula-images']);
        $modulaImagesSortedById = [];
        foreach ($postMetaArray['modula-images'] as $modulaImage) {
          $modulaImagesSortedById[$modulaImage['id']] = $modulaImage;
        }
        $postMetaArray['modula-images'] = $modulaImagesSortedById;
        $modulaGalleryArray['postmeta'] = $postMetaArray;
        $modulaGalleryArray['attachment_image_ids'] = $modulaGalleryImageIdArray;

        $post_id = $modulaGalleryArray['post_id'];
        $modulaGalleriesArray[$post_id] = $modulaGalleryArray;
      }
    }
    return $modulaGalleriesArray;
  }

  /**
   * Fetches the image items from an xml file.
   *
   * @param \SimpleXMLElement $xml
   *   The xml object.
   * @param string $imagesXPath
   *   The images xpath selector.
   *
   * @return array
   *   The image items.
   */
  public function fetchImagesFromXml(\SimpleXMLElement $xml, string $imagesXPath = "/rss/channel/item[wp:post_type='attachment']"): array {
    $modulaImagesXmlArray = $xml->xpath($imagesXPath);

    $modulaImagesArray = [];
    if (!empty($modulaImagesXmlArray)) {
      foreach ($modulaImagesXmlArray as $modulaImageXmlElement) {
        $modulaImageArray = (array) $modulaImageXmlElement->children('wp', TRUE);
        $modulaImageArray = array_merge($modulaImageArray, (array) $modulaImageXmlElement->children());
        $post_id = $modulaImageArray['post_id'];
        $postMetaArray = [];
        foreach ($modulaImageArray['postmeta'] as $postmeta) {
          $postMetaArray[(string) $postmeta->children('wp', TRUE)->meta_key] = (string) $postmeta->children('wp', TRUE)->meta_value;
        }
        $postMetaArray['_wp_attachment_metadata'] = unserialize($postMetaArray['_wp_attachment_metadata']);
        $modulaImageArray['postmeta'] = $postMetaArray;
        $modulaImagesArray[$post_id] = $modulaImageArray;
      }
    }
    return $modulaImagesArray;
  }

  /**
   * Creates media entities from Image arrays.
   *
   * @param array $galleryImage
   *   The image data array.
   * @param array $gallery
   *   The gallery the image is from.
   */
  public function createMediaFromEntry(array $galleryImage, array $gallery): void {
    $galleryName = Constants::GALLERY_NAME_MAPPING[$gallery['title']];
    // We map the archive types to their non archive types here:
    $galleryId = Constants::GALLERY_POST_ID_MAPPING[$gallery['post_id']];

    $imageId = $galleryImage['post_id'];
    $imageSrcUrl = $galleryImage['attachment_url'];
    $imageFileName = $galleryName . ' ' . str_replace('/', '-', $galleryImage['title']) . '.jpg';
    $imageName = $galleryName . ' ' . $galleryImage['post_name'] . ' | ' . $galleryImage['title'];
    $imageAlt = $galleryImage['postmeta']['_wp_attachment_image_alt'];
    $imageCreatedDateTime = new \DateTime($galleryImage['post_date_gmt']);
    $imageCreated = $imageCreatedDateTime->getTimestamp();
    $imageTagsFromImage = [];
    $imageTagsFromGallery = [];
    // Get tags from the image:
    if (!empty($galleryImage['postmeta']['wpmf_img_tags'])) {
      $imageTagsFromImage = explode(', ', $galleryImage['postmeta']['wpmf_img_tags']);
    }
    // Get tags from the gallery:
    if (!empty($gallery['postmeta']['modula-images'][$imageId]['filters'])) {
      $imageTagsFromGallery = explode(',', $gallery['postmeta']['modula-images'][$imageId]['filters']);
    }
    // Merge both:
    $imageSubCategorieSourceArray = array_merge($imageTagsFromImage, $imageTagsFromGallery);
    // Remove possible duplicates:
    $imageSubCategorieSourceArray = array_unique($imageSubCategorieSourceArray);

    $imageGalleryMainCategorie = Constants::GALLERY_MAIN_KATEGORIE_MAPPING[$galleryId];
    $imageGalleryCategories[$imageGalleryMainCategorie] = ['target_id' => $imageGalleryMainCategorie];
    foreach ($imageSubCategorieSourceArray as $imageSubCategorieSource) {
      $imageSubCategorieDestination = '';
      if ($galleryId == Constants::MAIN_KATEGORIE_REFERENZEN_SOURCE) {
        $imageSubCategorieDestination = Constants::GALLERY_SUB_KATEGORIE_REFERENZEN_MAPPING[$imageSubCategorieSource];
      }
      /* @phpstan-ignore-next-line */
      elseif ($galleryId == Constants::MAIN_KATEGORIE_BAUSTELLEN_SOURCE) {
        $imageSubCategorieDestination = Constants::GALLERY_SUB_KATEGORIE_BAUSTELLEN_MAPPING[$imageSubCategorieSource];
      }
      if (!empty($imageSubCategorieDestination)) {
        $imageGalleryCategories[$imageSubCategorieDestination] = ['target_id' => $imageSubCategorieDestination];
      }
    }
    $imageMediaTags[Constants::GALLERY_ID_TAG_MAPPING[$galleryId]] = ['target_id' => Constants::GALLERY_ID_TAG_MAPPING[$galleryId]];
    // Set WP-Import:
    $imageMediaTags[171] = ['target_id' => 171];
    if (empty($imageSubCategorieSourceArray)) {
      // Set WP-Import-Uncategorized:
      $imageMediaTags[172] = ['target_id' => 172];
    }
    $imageLabel = '';
    if (!empty($imageSubCategorieSourceArray)) {
      $imageLabel = $galleryImage['title'];
    }

    $directory = 'public://media/image/gallery/';
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $destination = $directory . $imageFileName;
    $imageBasename = $this->fileSystem->basename($destination);
    $remoteImageData = file_get_contents($imageSrcUrl);
    if (empty($remoteImageData)) {
      \Drupal::logger('wp_modula_gallery_xml_media_import')->warning('The image ' . $imageFileName . ' defines a non working url: ' . $imageSrcUrl . '!');
      return;
    }

    $image = $this->fileRepository->writeData($remoteImageData, $destination);
    $image->setFilename($imageBasename);
    $image->setOwnerId(\Drupal::currentUser()->id());
    $image->setMimeType('image/' . pathinfo($destination, PATHINFO_EXTENSION));
    $image->setPermanent();
    $image->save();

    $media_entity = Media::create([
      'bundle' => 'image',
      'uid' => \Drupal::currentUser()->id(),
      'name' => $imageName,
      'created' => $imageCreated,
      'status' => 1,
      'field_image' => [
        'target_id' => $image->id(),
        'alt' => $imageAlt,
        'title' => $imageLabel,
      ],
      'field_mime_type' => [
        'value' => $image->getMimeType(),
      ],
      'field_in_gallery' => array_values($imageGalleryCategories),
      'field_media_folder' => [
        'target_id' => '162',
      ],
      'field_media_tags' => array_values($imageMediaTags),
    ]);
    $media_entity->save();
  }

}

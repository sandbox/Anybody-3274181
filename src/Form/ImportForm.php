<?php

namespace Drupal\wp_modula_gallery_xml_media_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wp_modula_gallery_xml_media_import\XmlHandler;
use Drupal\wp_modula_gallery_xml_media_import\Constants;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles Wordpress Modula Gallery XML Export to Media Images ImportForm.
 */
class ImportForm extends FormBase {

  /**
   * The form builder.
   *
   * @var \Drupal\wp_modula_gallery_xml_media_import\XmlHandler
   */
  protected $xmlHandler;

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'wp_modula_gallery_xml_media_import_form';
  }

  /**
   * ModalFormContactController constructor.
   *
   * @param \Drupal\wp_modula_gallery_xml_media_import\XmlHandler $xmlHandler
   *   The xml handler.
   */
  public function __construct(XmlHandler $xmlHandler) {
    $this->xmlHandler = $xmlHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ImportForm {
    return new static(
      $container->get('wp_modula_gallery_xml_media_import.xml_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['import_images_and_galleries_wrapper'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Import Images and Galleries'),
      '#description' => $this->t('Make sure, your file is residing in your private file directory and the Constant is set to the file name.'),
    ];

    $form['import_images_and_galleries_wrapper']['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#weight' => 100,
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->parseWpImagesToMedia(Constants::XML_FILE_NAME);
  }

  /**
   * Parses Wordpress xml to media images.
   *
   * @param string $fileName
   *   The file name of in the private folder stored xml file.
   */
  public function parseWpImagesToMedia(string $fileName): void {
    $xml = $this->xmlHandler->convertXmlFileToObject($fileName);
    $galleries = $this->xmlHandler->fetchGalleriesFromXml($xml);
    $images = $this->xmlHandler->fetchImagesFromXml($xml);
    if (empty($galleries)) {
      throw new \Exception('No galleries found!');
    }
    if (empty($images)) {
      throw new \Exception('No images found!');
    }

    // Poolabdeckungen / Pooldecks Impressionen:
    unset($galleries['13284']);
    // Montagesituationen (3D) - Slideshow:
    unset($galleries['10187']);

    foreach ($galleries as $gallery) {
      $galleryImageIds = $gallery['attachment_image_ids'];
      foreach ($galleryImageIds as $galleryImageId) {
        if (empty($images[$galleryImageId])) {
          throw new \Exception('Image with ID ' . $galleryImageId . ' not found!');
          \Drupal::logger('wp_modula_gallery_xml_media_import')->warning('Image with ID ' . $galleryImageId . ' not found!');
        }
        $galleryImage = $images[$galleryImageId];
        $this->xmlHandler->createMediaFromEntry($galleryImage, $gallery);
      }
    }
  }

}

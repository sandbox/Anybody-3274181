<?php

namespace Drupal\wp_modula_gallery_xml_media_import;

/**
 * Constants for Wordpress Modula Gallery XML Export to Media Images.
 */
class Constants {

  /**
   * Import file name (in private:// directory).
   */
  public const XML_FILE_NAME = 'wp-modula-gallery-xml-media-import.xml';

  public const MAIN_KATEGORIE_REFERENZEN_SOURCE = '8404';
  public const MAIN_KATEGORIE_BAUSTELLEN_SOURCE = '8418';

  public const GALLERY_POST_ID_MAPPING = [
    // Referenzen -> Referenzen:
    self::MAIN_KATEGORIE_REFERENZEN_SOURCE => self::MAIN_KATEGORIE_REFERENZEN_SOURCE,
    // Baustellen -> Baustellen:
    self::MAIN_KATEGORIE_BAUSTELLEN_SOURCE => self::MAIN_KATEGORIE_BAUSTELLEN_SOURCE,
    // Referenzen Archiv -> Referenzen:
    '10860' => self::MAIN_KATEGORIE_REFERENZEN_SOURCE,
    // Baustellen Archiv -> Baustellen:
    '10861' => self::MAIN_KATEGORIE_BAUSTELLEN_SOURCE,
  ];

  public const GALLERY_NAME_MAPPING = [
    'Baustellen > Archiv' => 'Baustellen',
    'Referenzen > ARCHIV' => 'Archiv',
    'Baustellen' => 'Baustellen',
    'Referenzen' => 'Referenzen',
  ];

  public const GALLERY_MAIN_KATEGORIE_MAPPING = [
    // Referenzen -> Referenzen:
    self::MAIN_KATEGORIE_REFERENZEN_SOURCE => '102',
    // Baustellen -> Baustellen:
    self::MAIN_KATEGORIE_BAUSTELLEN_SOURCE => '103',
    // Referenzen Archiv -> Referenzen:
    '10860' => '102',
    // Baustellen Archiv -> Baustellen:
    '10861' => '103',
  ];

  public const GALLERY_SUB_KATEGORIE_REFERENZEN_MAPPING = [
    'Keramik- Beton- &amp; Stein Platten' => '110',
    'Keramik- Beton- & Stein Platten' => '110',
    'WPC Dielen' => '111',
    'Holz Dielen' => '112',
    'Balkon' => '113',
    'Pool' => '114',
    'Poolumrandung' => '121',
    'Poolterrasse' => '121',
    'Garten' => '115',
    'Aufgeständert' => '116',
    'Aufgeständerte Terrassen' => '116',
    'Flachdach' => '117',
    'Steg' => '118',
    'Podest' => '119',
    'Sauna Terrassen' => '122',
    'Treppe' => '124',
    'ALUECOFIX für Platten' => '166',
    'ALUECOFIX für Dielen' => '167',
    'Easy Construct' => '168',
  ];

  public const GALLERY_SUB_KATEGORIE_BAUSTELLEN_MAPPING = [
    'Keramik- Beton- &amp; Stein Platten' => '129',
    'Keramik- Beton- & Stein Platten' => '129',
    'WPC Dielen' => '104',
    'Holz Dielen' => '130',
    'Balkon' => '105',
    'Pool' => '131',
    'Poolumrandung' => '136',
    'Poolterrasse' => '136',
    'Garten' => '106',
    'Aufgeständert' => '132',
    'Aufgeständerte Terrassen' => '132',
    'Flachdach' => '133',
    'Steg' => '134',
    'Podest' => '135',
    'Sauna Terrassen' => '137',
    'Treppe' => '138',
    'Verlegung Platten auf Plattenlager' => '125',
    'ALUECOFIX für Platten' => '126',
    'ALUECOFIX für Dielen' => '127',
    'Easy Construct' => '128',
  ];

  public const GALLERY_ID_TAG_MAPPING = [
    self::MAIN_KATEGORIE_REFERENZEN_SOURCE => 169,
    self::MAIN_KATEGORIE_BAUSTELLEN_SOURCE => 170,
  ];

}
